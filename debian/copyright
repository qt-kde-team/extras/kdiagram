Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kdiagram
Source: https://cgit.kde.org/kdiagram.git

Files: *
Copyright: 2019-2020, Dag Andersen <danders@get2net.dk>
           2001-2015, Klaralvdalens Datakonsult AB. All rights reserved.
License: GPL-2+

Files: src/KGantt/test/TestKGanttConstraint.cpp
       src/KGantt/test/TestKGanttConstraint.h
       src/KGantt/test/TestKGanttConstraintModel.cpp
       src/KGantt/test/TestKGanttConstraintModel.h
       src/KGantt/test/TestKGanttView.cpp
       src/KGantt/test/TestKGanttView.h
       src/KGantt/test/TestMultiItems.cpp
       src/KGantt/test/TestMultiItems.h
Copyright: 2018-2020, Dag Andersen <danders@get2net.dk>
License: LGPL-2+

Files: .gitlab-ci.yml
       .kde-ci.yml
Copyright: None
License: CC0-1.0

Files: debian/*
Copyright: 2016 Sebastian Ramacher <sramacher@debian.org>
           2024 Patrick Franz <deltaone@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: CC0-1.0
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS DOCUMENT OR
 THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR
 DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator and
 subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 On Debian systems, the complete text of the Creative Commons Zero v1.0 Universal
 license can be found in "/usr/share/common-licenses/CC0-1.0".
